//
//  SideMenuView.swift
//  MyWeatherApp
//
//  Created by georg syncov on 02/04/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit
@IBDesignable
class SideMenuView: UIView {
    
    @IBInspectable var borderWight: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWight
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var viewRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = viewRadius
        }
    }
    @IBInspectable var shadowOffset: CGSize = CGSize(){
        didSet {
            self.layer.shadowOffset = shadowOffset
        }
    }
    @IBInspectable var shadowOpacity: Float = 0.0 {
        didSet {
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0.0 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
    @IBInspectable var shadowColor: UIColor = .clear {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    override func didMoveToWindow() {
        self.clipsToBounds = true
    }
    
    
    
   
}
