//
//  VerticalWeatherView.swift
//  MyWeatherApp
//
//  Created by georg syncov on 13/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class VerticalWeatherView: UIView {
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var cityNameLabel: UILabel!
    
    @IBOutlet weak var dayOfWeekLabel: UILabel!
    
    @IBOutlet weak var shortWeatherDescriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var windSpeedLabel: UILabel!
    
    var cityCoord: Coord?
    
    override func didMoveToWindow() {
    }
    
    
    func setWeather(weather: MyWeather){
        if let coord = weather.coord {
            self.cityCoord = coord
        }
        self.cityNameLabel.text = weather.name ?? ""
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        self.dayOfWeekLabel.text = dateFormatter.string(from: date)
        if let temp = weather.main?.temp {
            self.tempLabel.text = "\(Int(temp))"
            
        }
        if let description = weather.weather?.first?.main{
            self.shortWeatherDescriptionLabel.text = description
        }
        if let windSpeed = weather.wind?.speed{
            self.windSpeedLabel.text = "Wind speed is \(Int(windSpeed)) meter/sec"
        }

        
        if let iconName = weather.weather?.first?.icon{
            let suffix = iconName.suffix(1)
            if(suffix == "n"){
                self.backgroundColor = .lightGray
            }else{
                self.backgroundColor = .blue
            }
            self.weatherImageView.image = UIImage(named: iconName)
            self.weatherImageView.contentMode = .scaleAspectFill
        }
    }
 
}
extension UIView{
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "\(self)", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
