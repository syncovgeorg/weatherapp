//
//  SelectCityTableViewCell.swift
//  MyWeatherApp
//
//  Created by georg syncov on 20/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit



class SelectCityTableViewCell: UITableViewCell {
    @IBOutlet weak var countryFlagImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setCountryInfo(city: City){
        let text = "Untitled"
        nameLabel.text = "\(city.name ?? text), \(city.country ?? text)"
        if let country = city.country {
            countryFlagImageView.image = UIImage(named: "\(country)")
            countryFlagImageView.contentMode = .scaleAspectFit
        }
        
        
    }
    
}
