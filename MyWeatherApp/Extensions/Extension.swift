//
//  Extension.swift
//  MyWeatherApp
//
//  Created by georg syncov on 31/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation

extension Date{
    static func getFormattedDate()->String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd\nHH:mm"
        return dateFormat.string(from: Date())
    }
}
