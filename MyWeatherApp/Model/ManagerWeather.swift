//
//  ManagerWeather.swift
//  MyWeatherApp
//
//  Created by georg syncov on 26/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation

class ManagerWeather {
    
    static let shared = ManagerWeather()
    private init(){}
    
    func getMyWeatherToFirstView(completion: @escaping (_ testWeather: MyWeather)->()){
        Routers.shared.getWeatherJSONFromCoord { [weak self] result, error in
            if let weather = result {
                print("get weather from coord")
                completion(weather)
            }
            else {
                if let error = error {
                    print(error)
                }
            }
        }
//        if Cash.shared.isUserTimeIntervalForPasswordRequestExceeded(){
//            Routers.shared.getWeatherJSONFromCoord { [weak self] result, error in
//                if let weather = result {
//                    print("get weather from coord")
//                    completion(weather)
//                }
//                else {
//                    if let error = error {
//                        print(error)
//                    }
//                }
//            }
//        } else {
//            if let data = Cash.shared.getPrevWeatherForMyLocation(){
//                do{
//                    let myWeather = try JSONDecoder().decode(MyWeather.self, from: data)
//                    print("get weather from cash")
//                    completion(myWeather)
//
//                } catch {
//
//                }
//
//            }
//        }
//        print("return testWeather")
    }
    
    func getWeatherToSeveralId(completion: @escaping (_ weatherForSeveralId: [MyWeather])->()){
            Routers.shared.getWeatherJSONToSeveralCitiesFromId { [weak self] result, error in
                if let citiesList = result {
                    print("get weather from coord")
                    if let listWeather = citiesList.list{
                      completion(listWeather)
                    }
                }
                else {
                    if let error = error {
                        print(error)
                    }
                }
            }
        print("return testWeather")
    }
    

}
