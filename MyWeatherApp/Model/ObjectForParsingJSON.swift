//
//  City.swift
//  MyWeatherApp
//
//  Created by georg syncov on 19/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import CoreLocation

class CitiesList: Codable{
    var cnt: Int?
    var list: [MyWeather]?
}

class City: Codable {
    var id: Int?
    var name: String?
    var country: String?
    var coord: Coord?

    func getCityNameLowcassed()->String{
        return self.name?.lowercased() ?? ""
    }
    func getCityId()->Int {
        return self.id ?? 0
    }
}

class Coord: Codable{
    var lon: Double?
    var lat: Double?
}

class MyWeather: Codable {
    
    var name: String?
    var main: Main?
    var weather: [Weather]?
    var wind: Wind?
    var coord: Coord?
}
class Main: Codable {
    var humidity: Double?
    var pressure: Double?
    var temp: Double?
}
class Weather: Codable {
    var icon: String?
    var main: String?
}
class Wind: Codable {
    var speed: Double?
}







