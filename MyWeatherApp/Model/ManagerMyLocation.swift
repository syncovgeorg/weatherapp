//
//  LocationManager.swift
//  MyWeatherApp
//
//  Created by georg syncov on 14/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import CoreLocation

class ManagerMyLocation {
    
    static let shared = ManagerMyLocation()
    private init() {}
    
    var userLocation: CLLocation?

    func setUserLocation(_ value: CLLocation){
        userLocation = value
    }
    func getUserLocation()->CLLocation?{
        return userLocation
    }
    
    
}




