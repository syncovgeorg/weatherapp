//
//  Routers.swift
//  MyWeatherApp
//
//  Created by georg syncov on 14/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import CoreLocation

struct UserKey {
    static let userApiKey: String = "aeb46271e330b0e38e34ba813a0c6a4f"
}


class Routers {
    static let shared = Routers()
    private init(){}
    
    
    func getWeatherJSONFromCoord(completion: @escaping (_ result: MyWeather?, _ error: Error?)->()){
        print("request from coord start start")
        
        let lat = ManagerMyLocation.shared.userLocation?.coordinate.latitude ?? 0
        let lon = ManagerMyLocation.shared.userLocation?.coordinate.longitude ?? 0
        print(lat,lon)
        
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(UserKey.userApiKey)&units=metric") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let data = data {
                do{
                    Cash.shared.setTimeLastCoordRequest()
                    Cash.shared.setPrevWeather(weather: data)
                    let myWeather = try JSONDecoder().decode(MyWeather.self, from: data)
                    completion(myWeather, nil)
                    
                } catch {
                    print(error)
                    completion(nil, error)
                }
            }
        }
        task.resume()
    }
    
    func getWeatherJSONToSeveralCitiesFromId(completion: @escaping (_ result: CitiesList?, _ error: Error?)->()){
        print("request from several cities start")
        
       let severalId = Cash.shared.getSeveralIdForRequest()
        
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/group?id=\(severalId)&appid=\(UserKey.userApiKey)&units=metric") else {
            return}

        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let data = data {
                do{
                    print("request from several cities data ok")
                    let listCitiesWeather = try JSONDecoder().decode(CitiesList.self, from: data)
                    completion(listCitiesWeather, nil)
                    
                } catch {
                    print("request from several cities \(error)")
                    completion(nil, error)
                }
            }
        }
        print("request from several cities finished")
        task.resume()
    }
    
    func getCityInfoFromJSON(finishedGetCityinfo: (_ result: [City])->()){
        var cityList = [City]()
        if let path = Bundle.main.path(forResource: "cityList", ofType: "json") {
            print("parsing start")
            if let data = FileManager.default.contents(atPath: path){
                do {
                    print("data ok")
                    cityList = try JSONDecoder().decode([City].self, from: data)
                    
                } catch {
                    print("error json parsing")
                }
            }
            print("parsing stop")
        }
        finishedGetCityinfo(cityList)
        
    }
    
    
}
