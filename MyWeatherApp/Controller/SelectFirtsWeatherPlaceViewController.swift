protocol SelectFirtsWeatherPlaceViewControllerDelegate{
    func updateFirstPlaceWeather(value: Bool)
}

import UIKit

class SelectFirtsWeatherPlaceViewController: UIViewController {
    
    @IBOutlet weak var selectFirstWeatherCity: UITableView!
    @IBOutlet weak var searhBar: UISearchBar!
    
    var cityArray = [City]()
    var currentCityArrayForSeaching = [City]()
    var delegate: SelectFirtsWeatherPlaceViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cityArray = Cash.shared.getCityList()
        currentCityArrayForSeaching = cityArray
        setSearchBar()
        
    }
    
    private func setSearchBar() {
        searhBar.delegate = self
        searhBar.placeholder = "Select city to first weather display "
    }
}


extension SelectFirtsWeatherPlaceViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentCityArrayForSeaching.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "selectCityCell", for: indexPath) as? SelectCityTableViewCell else {
            return UITableViewCell()
        }
        cell.setCountryInfo(city: currentCityArrayForSeaching[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        Cash.shared.setFirstCityToShowWeather(city: currentCityArrayForSeaching[indexPath.row])
        delegate?.updateFirstPlaceWeather(value: true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}
extension SelectFirtsWeatherPlaceViewController: UISearchBarDelegate{
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //поработать с исключением пробелов
        if searchText.count > 1 {
            let text = searchText.lowercased()
            currentCityArrayForSeaching = cityArray.filter( { city -> Bool in
                (city.getCityNameLowcassed().contains(text))
            })
            selectFirstWeatherCity.reloadData()
            
        } else { currentCityArrayForSeaching = cityArray
            selectFirstWeatherCity.reloadData()
            return}
        
    }
    
}
