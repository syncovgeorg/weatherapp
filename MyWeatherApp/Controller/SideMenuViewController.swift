//
//  SideMenuViewController.swift
//  MyWeatherApp
//
//  Created by georg syncov on 20/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var settingsTitleLabel: UILabel!
    @IBOutlet weak var settinglTitleView: UIView!
    @IBOutlet weak var favoritPlacesView: UIView!
    @IBOutlet weak var favoritPlacesLabel: UILabel!
    @IBOutlet weak var intervalToUpgtadeLabel: UILabel!
    @IBOutlet weak var intervalToUpgradeView: UIView!
    @IBOutlet weak var firstShowView: UIView!
    @IBOutlet weak var firstShowLabel: UILabel!
    @IBOutlet weak var favoritPlacesTableView: UITableView!
    
    var delegate : SelectFirtsWeatherPlaceViewControllerDelegate?
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favoritPlacesTableView.delegate = self
        favoritPlacesTableView.dataSource = self
        setTextIntervalToUpgtadeLabel()
        setTextOnFirstShowLabel()
        setTextOnFirstShowLabel()
    }
    
    @IBAction func selectCityButtonPressed(_ sender: Any) {
        goToSelectCityVC()
    }
    
    @IBAction func setIntervalToupgradeWeatherButton(_ sender: UIButton) {
        showAlertToSetTimeIntervalToUpgrade()
    }
    
    @IBAction func setFirstShowWeatherButton(_ sender: UIButton) {
        showAlertToFirstWeatherPlace()
    }
    
    func goToSelectCityVC(){
        guard let selectCityVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectCityViewController") as? SelectCityViewController else {return}
        self.navigationController?.pushViewController(selectCityVC, animated: true)
        AppDelegate.isSideMenuViewController = false
    }
    func goToFirstWeatherPlaceSelectVC(){
        guard let selectFirstWeatherVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectFirtsWeatherPlaceViewController") as? SelectFirtsWeatherPlaceViewController else {return}
        selectFirstWeatherVC.delegate = self.delegate
        AppDelegate.isSideMenuViewController = false
        self.navigationController?.pushViewController(selectFirstWeatherVC, animated: true)
        
    }
    
    func setTextIntervalToUpgtadeLabel(){
        intervalToUpgtadeLabel.text = "Interval to upgrade weather : \(Cash.shared.getUpgrateTimeIntervalInSecunds() / (60*60)) h"
    }
    
    func setTextOnFirstShowLabel(){
        var text = ""
        if Cash.shared.getIsFirstPlaseInCoord(){
            text = "my coordinate"
        } else {
            if let city = Cash.shared.getFirstCityToShowWeather(){
                text = "city \(city.name ?? "Untitled")"
            }
        }
        firstShowLabel.text = "First show weather on: \(text)"
    }
    
    
    func showAlertToSetTimeIntervalToUpgrade(){
        let alert = UIAlertController(title: "Time interval", message: "Select time interval to upgrate weather", preferredStyle: .actionSheet)
        let oneHourButton = UIAlertAction(title: "1 hour", style: .default) { (_) in
            Cash.shared.setUpgrateTimeIntervalInSecunds(intervalInSec: (60*60))
            self.setTextIntervalToUpgtadeLabel()
        }
        let twoHourButton = UIAlertAction(title: "2 hours", style: .default) { (_) in
            Cash.shared.setUpgrateTimeIntervalInSecunds(intervalInSec: (2*60*60))
            self.setTextIntervalToUpgtadeLabel()
        }
        let fourHourButton = UIAlertAction(title: "4 hours", style: .default) { (_) in
            Cash.shared.setUpgrateTimeIntervalInSecunds(intervalInSec: (4*60*60))
            self.setTextIntervalToUpgtadeLabel()
        }
        let eightHourButton = UIAlertAction(title: "8 hours", style: .default) { (_) in
            Cash.shared.setUpgrateTimeIntervalInSecunds(intervalInSec: (8*60*60))
            self.setTextIntervalToUpgtadeLabel()
        }
        alert.addAction(oneHourButton)
        alert.addAction(twoHourButton)
        alert.addAction(fourHourButton)
        alert.addAction(eightHourButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertToFirstWeatherPlace(){
        let alert = UIAlertController(title: "First weather place", message: "Make a selection for the first weather display", preferredStyle: .alert)
        let coord = UIAlertAction(title: "My curent coordinate", style: .default) { (_) in
            Cash.shared.setIsFirstPlaseInCoord(true)
            self.setTextOnFirstShowLabel()
        }
        let city = UIAlertAction(title: "Selected city", style: .default) { (_) in
            Cash.shared.setIsFirstPlaseInCoord(false)
            self.goToFirstWeatherPlaceSelectVC()
        }
        
        alert.addAction(coord)
        alert.addAction(city)
        self.present(alert, animated: true, completion: nil)
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cash.shared.getSelectedCities()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "selectCityCell", for: indexPath) as? SelectCityTableViewCell else {
            return UITableViewCell()
        }
        if let city = Cash.shared.getSelectedCities()?[indexPath.row] {
            cell.setCountryInfo(city: city)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Favorit places"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
