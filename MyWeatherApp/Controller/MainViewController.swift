//
//  MainViewController.swift
//  MyWeatherApp
//
//  Created by georg syncov on 14/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//
import Foundation
import UIKit
import CoreLocation

class MainViewController: UIViewController {
    
    
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var topNavBarView: UIView!
    
    let locationManager = CLLocationManager()
    var sideMenuViewController = SideMenuViewController()
    var weatherForFavoritePlaces = [MyWeather]()
    var isFirstWeatherViewOnScreen = Bool()
    var indexOfFavoritPlace = 0
    
    var weatherView = { () -> VerticalWeatherView in
        let view = VerticalWeatherView.instanceFromNib() as! VerticalWeatherView
        return view
    }()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setFirstWeatherView()
        getWeatherForFavoritePlaces()
        sideMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        if !AppDelegate.isSideMenuViewController {
            showSideMenu()
        }
    }
    
    //MARK: side menu function
    @IBAction func sideMenuButtonPressed(_ sender: UIButton) {
        if AppDelegate.isSideMenuViewController {
            showSideMenu()
        } else {
            hideSideMenu()
        }
    }
    
    func showSideMenu() {
        self.sideMenuViewController.view.frame = CGRect(origin: UserConstants.startPrevViewPoint, size: CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - self.topNavBarView.frame.height))
        self.addChild(self.sideMenuViewController)
        self.view.addSubview(sideMenuViewController.view)
        self.sideMenuViewController.delegate = self
        AppDelegate.isSideMenuViewController = false
        UIView.animate(withDuration: 0.3) {
            self.sideMenuViewController.view.frame.origin = UserConstants.finishPoint
        }
    }
    
    func hideSideMenu(){
        UIView.animate(withDuration: 0.3, animations: {
             self.sideMenuViewController.view.frame.origin = UserConstants.startPrevViewPoint
        }) { (_) in
            AppDelegate.isSideMenuViewController = true
            self.sideMenuViewController.view.removeFromSuperview()
        }
    }

    //MARK: weather view function
    func setFirstWeatherView(){
        isFirstWeatherViewOnScreen = true
        weatherView.frame = CGRect(origin: UserConstants.finishPoint, size: CGSize(width: self.view.bounds.width, height: self.view.bounds.height - topNavBarView.frame.height))
        addGestureRecogniser(view: weatherView)
        indexOfFavoritPlace = 0
        self.view.addSubview(weatherView)
        updateWeather(Cash.shared.isUserTimeIntervalForPasswordRequestExceeded())
    }
    
    func updateWeather(_ isIntervalExceeded: Bool){
        
        if isIntervalExceeded
        {
            if Cash.shared.getIsFirstPlaseInCoord(){
            updateCoordinate()
            } else {
                if let city = Cash.shared.getFirstCityToShowWeather(){
                    let location = CLLocation(latitude: city.coord?.lat ?? 0, longitude: city.coord?.lon ?? 0)
                    ManagerMyLocation.shared.setUserLocation(location)
                    ManagerWeather.shared.getMyWeatherToFirstView {[weak self] testWeather in
                        DispatchQueue.main.async {
                            self?.weatherView.setWeather(weather: testWeather)
                        }
                    }
                }
            }
        } else {
            ManagerWeather.shared.getMyWeatherToFirstView {[weak self] testWeather in
                DispatchQueue.main.async {
                    self?.weatherView.setWeather(weather: testWeather)
                }
            }
        }
        
    }
    
    func updateCoordinate(){
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func getWeatherForFavoritePlaces(){
        ManagerWeather.shared.getWeatherToSeveralId {[weak self] weatherForSeveralId in
            DispatchQueue.main.async {
                self?.weatherForFavoritePlaces = weatherForSeveralId
            }
        }
    }
    
    func showWeatherOnNextPlace(swipe: UISwipeGestureRecognizer){
        guard let presentView = swipe.view else {return}
        let nextWeatherView = VerticalWeatherView.instanceFromNib() as! VerticalWeatherView
        nextWeatherView.frame.size = CGSize(width: self.view.bounds.width, height: self.view.bounds.height - topNavBarView.frame.height)
        addGestureRecogniser(view: nextWeatherView)
        
        if !weatherForFavoritePlaces.isEmpty  {
            switch swipe.direction.rawValue{
            case 2 :
                if isFirstWeatherViewOnScreen{
                    indexOfFavoritPlace = weatherForFavoritePlaces.startIndex
                } else {
                    indexOfFavoritPlace += 1
                }
                if weatherForFavoritePlaces.indices.contains(indexOfFavoritPlace){
                    nextWeatherView.frame.origin = UserConstants.startNextViewPoint
                    nextWeatherView.setWeather(weather: weatherForFavoritePlaces[indexOfFavoritPlace])
                    self.view.addSubview(nextWeatherView)
                    UIView.animate(withDuration: 0.3, animations: {
                        nextWeatherView.frame.origin = UserConstants.finishPoint
                        presentView.frame.origin = UserConstants.startPrevViewPoint
                    }) { (_) in
                        self.isFirstWeatherViewOnScreen = false
                        print(self.indexOfFavoritPlace)
                        presentView.removeFromSuperview()
                    }
                } else {
                    presentView.removeFromSuperview()
                    self.setFirstWeatherView()
                }
            case 1:
                if isFirstWeatherViewOnScreen{
                    indexOfFavoritPlace = weatherForFavoritePlaces.endIndex - 1
                } else {
                    indexOfFavoritPlace -= 1
                }
                if weatherForFavoritePlaces.indices.contains(indexOfFavoritPlace){
                    nextWeatherView.frame.origin = UserConstants.startPrevViewPoint
                    nextWeatherView.setWeather(weather: weatherForFavoritePlaces[indexOfFavoritPlace])
                    self.view.addSubview(nextWeatherView)
                    UIView.animate(withDuration: 0.3, animations: {
                        nextWeatherView.frame.origin = UserConstants.finishPoint
                        presentView.frame.origin = UserConstants.startNextViewPoint
                    }) { (_) in
                        self.isFirstWeatherViewOnScreen = false
                        print(self.indexOfFavoritPlace)
                        presentView.removeFromSuperview()
                    }
                } else {
                    presentView.removeFromSuperview()
                    setFirstWeatherView()
                }
                
            default: break
            }
        }
    }
    
    //MARK: gesture function
    func addGestureRecogniser(view: VerticalWeatherView){
        
        let leftSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(horizontalSwipeRecognized(_:)))
        leftSwipeRecognizer.direction = .left
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(horizontalSwipeRecognized(_:)))
        rightSwipeRecognizer.direction = .right
        view.addGestureRecognizer(leftSwipeRecognizer)
        view.addGestureRecognizer(rightSwipeRecognizer)
    }
    
    @IBAction func horizontalSwipeRecognized(_ sender: UISwipeGestureRecognizer) {
        showWeatherOnNextPlace(swipe: sender)
        print(sender.direction)
    }
    
    
}



extension MainViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0] as CLLocation
        ManagerMyLocation.shared.setUserLocation(location)
        ManagerWeather.shared.getMyWeatherToFirstView {[weak self] testWeather in
            DispatchQueue.main.async {
                self?.weatherView.setWeather(weather: testWeather)
            }
        }
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension MainViewController: SelectFirtsWeatherPlaceViewControllerDelegate{
    func updateFirstPlaceWeather(value: Bool){
        self.updateWeather(value)
        print("delegate work")
    }
}





