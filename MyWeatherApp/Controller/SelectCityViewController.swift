//
//  SelectCityViewController.swift
//  MyWeatherApp
//
//  Created by georg syncov on 20/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class SelectCityViewController: UIViewController {
    
    @IBOutlet weak var selectCityTableView: UITableView!
    @IBOutlet weak var searchCityBar: UISearchBar!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    var isEditingEnabled = Bool(){
        didSet {
            if !isEditingEnabled {
                editButton.backgroundColor = .clear
                selectCityTableView.isEditing = false
            } else {
              editButton.backgroundColor = .red
                selectCityTableView.isEditing = true
            }
        }
    }
    var cityArray = [City]()
    var currentCityArrayForSeaching = [City]()
    var selectedCities = [City]()
    var activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cityArray = Cash.shared.getCityList()
        selectedCities = Cash.shared.getSelectedCities() ?? [City]()
        currentCityArrayForSeaching = cityArray
        setSearchBar()
        isEditingEnabled = false
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        isEditingEnabled = !isEditingEnabled
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        Cash.shared.setSelectedCities(selectedCities)
        self.navigationController?.popToRootViewController(animated: true)
    }
    //не окончено, доработать
    func startStopActivityIndicator(_ text: String){
        switch text {
        case "start":
            activityIndicatorView.color = .red
            self.view.addSubview(activityIndicatorView)
            activityIndicatorView.frame = self.view.frame
            activityIndicatorView.center = self.view.center
            activityIndicatorView.startAnimating()
        default:
            activityIndicatorView.stopAnimating()
            activityIndicatorView.removeFromSuperview()
        }
        
        
    }
    
    private func setSearchBar() {
        searchCityBar.delegate = self
        searchCityBar.placeholder = "Find cities from name"
    }
    
    private func addCityToSelectedCities(selectedCity: City){
          if selectedCities.endIndex < 5 && !selectedCities.contains { city -> Bool in
                (city.getCityId() == selectedCity.getCityId())}
            {
                selectedCities.append(selectedCity)
                selectCityTableView.reloadData()
            }
            if selectedCities.endIndex >= 5 {
                showCautionAlert()
            }
    }
    
    func showCautionAlert(){
        let alert = UIAlertController(title: "Caution", message: "Maximum of favorite places is 5, please remove something places", preferredStyle: .alert)
        let button = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(button)
        self.present(alert, animated: true, completion: nil)
    }
}

extension SelectCityViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedCities.isEmpty{
            return 1
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !selectedCities.isEmpty{
            if section == 0 {
               
                return "Selected cities"
            } else {
                
                return "All cities"
            }
        } else {
            return "All cities"
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !selectedCities.isEmpty{
            if section == 0 {
                return selectedCities.count
            } else {
                return currentCityArrayForSeaching.count
            }
        } else {
            return currentCityArrayForSeaching.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "selectCityCell", for: indexPath) as? SelectCityTableViewCell else {
            return UITableViewCell()
        }
        if !selectedCities.isEmpty{
            if indexPath.section == 0
            {
                cell.setCountryInfo(city: selectedCities[indexPath.row])
                cell.accessoryType = .none
                
            }
            if indexPath.section == 1 {
                cell.setCountryInfo(city: currentCityArrayForSeaching[indexPath.row])
                if selectedCities.contains(where: { (city) -> Bool in
                    city.getCityId() == currentCityArrayForSeaching[indexPath.row].getCityId()
                }) {
                    cell.accessoryType = .checkmark
                }else  {cell.accessoryType = .none}
                
            }
        } else {
            cell.setCountryInfo(city: currentCityArrayForSeaching[indexPath.row])
             cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if !selectedCities.isEmpty && indexPath.section == 1{
                addCityToSelectedCities(selectedCity: currentCityArrayForSeaching[indexPath.row])
        } else {
            addCityToSelectedCities(selectedCity: currentCityArrayForSeaching[indexPath.row])
        }
        //проверка
        print(selectedCities.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UserConstants.heightOfTableCitiesSelectRows
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if !selectedCities.isEmpty{
            if indexPath.section == 0 {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            selectedCities.remove(at: indexPath.row)
            if selectedCities.isEmpty{
                selectCityTableView.reloadData()
            } else {
            selectCityTableView.deleteRows(at: [indexPath], with: .left)
            }
        }
    }

}

extension SelectCityViewController: UISearchBarDelegate{
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //поработать с исключением пробелов
        if searchText.count > 1 {
            let text = searchText.lowercased()
            currentCityArrayForSeaching = cityArray.filter( { city -> Bool in
                (city.getCityNameLowcassed().contains(text))
            })
            selectCityTableView.reloadData()
            
        } else { currentCityArrayForSeaching = cityArray
            selectCityTableView.reloadData()
            return}
        
    }
    
}
//MARK: добавить сдвиг таблицы при появлении клавиатуы поиска

