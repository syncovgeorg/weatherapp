//
//  Cash.swift
//  MyWeatherApp
//
//  Created by georg syncov on 20/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import UIKit

struct UserConstants {
    static let upgrateTimeIntervalInSecunds = 14400
    static let heightOfTableCitiesSelectRows: CGFloat = 50
    static let finishPoint = CGPoint(x: 0, y: 64)
    static let startNextViewPoint = CGPoint(x: UIScreen.main.bounds.width, y: 64)
    static let startPrevViewPoint = CGPoint(x: -UIScreen.main.bounds.width, y: 64)
}

class Cash {
    static let shared = Cash()
    private init(){}
    
    var isCitiesInfoLoading = false
    
    let defaults = UserDefaults.standard
    
    var cityList: [City]?
    var selectedCityList: [City]?
    var upgrateTimeIntervalInSecunds = 14400
    var isFirstPlaceInCoord = true
    var firstCityToShowWeather = City()
    
    func setFirstCityToShowWeather(city: City){
        if let data = try? JSONEncoder().encode(city){
            defaults.set(data, forKey: "firstCityToShowWeather")
        }
    }
    
    func getFirstCityToShowWeather()-> City?{
        guard let data = defaults.data(forKey: "firstCityToShowWeather") else {return nil}
        let city = try? JSONDecoder().decode(City.self, from: data)
        return city
    }
    
    func setIsFirstPlaseInCoord(_ change: Bool){
        defaults.set(change, forKey: "firstWeatherPlace")
    }
    
    func getIsFirstPlaseInCoord()-> Bool {
        return defaults.object(forKey: "firstWeatherPlace") as? Bool ?? true
    }
    
    
    func setUpgrateTimeIntervalInSecunds(intervalInSec: Int){
        defaults.set(intervalInSec, forKey: "upgrateTimeIntervalInSecunds")
    }
    
    func getUpgrateTimeIntervalInSecunds()->Int {
        return defaults.object(forKey: "upgrateTimeIntervalInSecunds") as? Int ?? upgrateTimeIntervalInSecunds
    }
    
    
    func setCityList(){
        if cityList == nil{
            Routers.shared.getCityInfoFromJSON {[weak self] result in
                self?.cityList = result
            }
        }
    }
    
    func getCityList() -> [City] {
        if cityList == nil {
            setCityList()
        }
        return cityList ?? [City]()
    }
    
    func getPrevWeatherForMyLocation()-> Data?{
        return defaults.data(forKey: "prevWeather")
        
    }
    func setPrevWeather(weather: Data){
        defaults.set(weather, forKey: "prevWeather")
    }
    
    func setTimeLastCoordRequest(){
        let dateNow = Int(Date().timeIntervalSinceReferenceDate)
        defaults.set(dateNow, forKey: "timeLastCoordRequest")
    }
    
    func getTimeLastCoordRequest()->Int?{
        return defaults.integer(forKey: "timeLastCoordRequest")
    }
    
    func isUserTimeIntervalForPasswordRequestExceeded()->Bool{
        var result = true
        let dateNow = Int(Date().timeIntervalSinceReferenceDate)
        if let dateLastCoordRequest = getTimeLastCoordRequest(){
            let timeInterval = dateNow - dateLastCoordRequest
            if timeInterval < getUpgrateTimeIntervalInSecunds() {
                result = false
            }
        }
       // return result
        return true
    }
    
    func getSelectedCities()->[City]?{
        guard let data = defaults.data(forKey: "selectedCities") else {return nil}
        let selectedCities = try? JSONDecoder().decode([City].self, from: data)
        return selectedCities
    }
    
    func setSelectedCities(_ cities: [City]) {
        if let data = try? JSONEncoder().encode(cities){
            defaults.set(data, forKey: "selectedCities")
        }
    }
    
  func getSeveralIdForRequest()->String{
        var severalId = ""
        if let selectedCities = getSelectedCities(){
            for city in selectedCities{
                severalId += "\(city.getCityId()),"
            }
            if severalId.count > 2{
                severalId.remove(at: severalId.index(before: severalId.endIndex))
            }
        }
        return severalId
    }
    
}
